package com.watergenerator.ui.adapter;

import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public abstract class ListAdapter<DataT, ViewHolderT extends ListAdapter.BaseViewHolder>
    extends RecyclerView.Adapter<ViewHolderT> {

  protected ArrayList<DataT> mItems = new ArrayList<>();
  protected OnItemClickListener<DataT> mListener;

  public ListAdapter(OnItemClickListener<DataT> listener) {
    mListener = listener;
  }

  @Override
  public ViewHolderT onCreateViewHolder(ViewGroup parent, int viewType) {
    return null;
  }

  @Override
  public void onBindViewHolder(ViewHolderT holder, int position) {
    holder.bind(mItems.get(position));
  }

  @Override
  public int getItemCount() {
    return mItems.size();
  }

  public void setItems(@NonNull ArrayList<DataT> items) {
    mItems = items;
    notifyDataSetChanged();
  }

  public void clearItems() {
    mItems.clear();
    notifyDataSetChanged();
  }

  public void addItem(DataT item) {
    mItems.add(item);
    notifyDataSetChanged();
  }

  public interface OnItemClickListener<DataT> {
    void onItemClick(DataT item);
  }

  public abstract static class BaseViewHolder<DataT> extends RecyclerView.ViewHolder {

    public BaseViewHolder(View itemView) {
      super(itemView);
    }

    public abstract void bind(DataT item);
  }
}
