package com.watergenerator.ui.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.Nullable;
import com.watergenerator.R;
import com.watergenerator.entity.WatterInfo;

public class ProductAdapter extends ListAdapter<WatterInfo, ProductAdapter.WatterHolder> {
  private Context context;

  public ProductAdapter(@Nullable OnItemClickListener<WatterInfo> listener) {
    super(listener);
  }

  @Override
  public WatterHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(parent.getContext())
        .inflate(R.layout.item_water, parent, false);
    context = parent.getContext();

    final WatterHolder viewHolder = new WatterHolder(view);

    view.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        if (mListener != null) {
          mListener.onItemClick(mItems.get(viewHolder.getAdapterPosition()));
        }
      }
    });

    return viewHolder;
  }

  @Override
  public void onBindViewHolder(WatterHolder holder, final int position) {
    super.onBindViewHolder(holder, position);

    holder.itemView.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
//        Answers.getInstance().logCustom(new CustomEvent("Клик по итему"));
        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + mItems.get(position).getContacts().getPhoneNumber()));
        if (intent.resolveActivity(context.getPackageManager()) != null) {
          context.startActivity(intent);
        }
      }
    });
  }

  public static class WatterHolder extends ListAdapter.BaseViewHolder<WatterInfo> {
    private ImageView mBottleImageView;
    private TextView mWatterNameTextView;
    private TextView mWatterTypeTextView;
    private TextView mStartWorkTimeTextView;
    private TextView mFinishWorkTimeTextView;
    private TextView mDashTextView;
    private TextView mPriceTextView;
    private ImageView mMessageStatusImageView;
    private ImageView mExpressDeliveryImageView;

    public WatterHolder(View itemView) {
      super(itemView);
      mBottleImageView = itemView.findViewById(R.id.img_bottle);
      mWatterNameTextView = itemView.findViewById(R.id.txv_name_water);
      mWatterTypeTextView = itemView.findViewById(R.id.txv_type_bottle);
      mStartWorkTimeTextView = itemView.findViewById(R.id.txv_time_start_work);
      mFinishWorkTimeTextView = itemView.findViewById(R.id.txv_time_end_work);
      mPriceTextView = itemView.findViewById(R.id.txv_price_water);
      mMessageStatusImageView = itemView.findViewById(R.id.img_message_status);
      mExpressDeliveryImageView = itemView.findViewById(R.id.img_fast_delivery_status);
    }

    @Override
    public void bind(WatterInfo item) {
      mBottleImageView.setImageDrawable(mBottleImageView.getContext().getDrawable(item.getLogoResId()));
      mWatterNameTextView.setText(item.getName());
      mWatterTypeTextView.setText(item.getReadableType());
      mPriceTextView.setText("" + (int)item.getPrice());
    }
  }
}
