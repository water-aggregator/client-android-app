package com.watergenerator;

import android.net.Uri;
import android.os.Bundle;

import android.view.MenuItem;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.google.android.material.navigation.NavigationView;
import com.crashlytics.android.Crashlytics;
import com.watergenerator.settings.Preferences;
import io.fabric.sdk.android.Fabric;


public class MainActivity extends AppCompatActivity
    implements NavigationView.OnNavigationItemSelectedListener,
    ChooseWaterFragment.OnFragmentInteractionListener,
    ChooseCityFragment.OnFragmentInteractionListener,
    AboutAppFragment.OnFragmentInteractionListener,
    RepeatOrderFragment.OnFragmentInteractionListener,
OnCitySelectedListener{

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    Preferences.initInstance(getApplicationContext());
    Fabric.with(this, new Crashlytics());
    setContentView(R.layout.activity_main_activity);
    Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);

    DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
    ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
        this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
    drawer.addDrawerListener(toggle);
    drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNDEFINED);
    toggle.syncState();

    NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
    navigationView.setNavigationItemSelectedListener(this);

    Fragment fragment = null;
    Class fragmentClass = null;
    if (!Preferences.getInstance().isCityInit()) {
      fragmentClass = ChooseCityFragment.class;
      getSupportActionBar().hide();
    } else {
      getSupportActionBar().show();
      replaceFragment();
    }

    try {
      fragment = (Fragment) fragmentClass.newInstance();
      ((ChooseCityFragment) fragment).setOnCitySelectedListener(this);
    } catch (Exception e) {
      e.printStackTrace();
    }
    if (fragment != null) {
      FragmentManager fragmentManager = getSupportFragmentManager();
      fragmentManager.beginTransaction().replace(R.id.container, fragment).commit();
    }
  }

  @Override
  public void onBackPressed() {
    DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
    if (drawer.isDrawerOpen(GravityCompat.START)) {
      drawer.closeDrawer(GravityCompat.START);
    } else {
      super.onBackPressed();
    }
  }

  @Override
  public boolean onNavigationItemSelected(MenuItem item) {
    Fragment fragment = null;
    Class fragmentClass = null;

    // Handle navigation view item clicks here.
    int id = item.getItemId();

    if (id == R.id.nav_order_water) {
      Answers.getInstance().logCustom(new CustomEvent("Выбран фрагмент 'Заказать воду'"));
      fragmentClass = ChooseWaterFragment.class;
    } else if (id == R.id.nav_repeat_order) {
      Answers.getInstance().logCustom(new CustomEvent("Выбран фрагмент 'Повторить заказ'"));
      fragmentClass = RepeatOrderFragment.class;
    } else if (id == R.id.nav_change_city) {
      Answers.getInstance().logCustom(new CustomEvent("Выбран фрагмент 'Изменить город'"));
      fragmentClass = ChooseCityFragment.class;
      getSupportActionBar().hide();
    } else if (id == R.id.nav_about_app) {
      Answers.getInstance().logCustom(new CustomEvent("Выбран фрагмент 'О приложении'"));
      fragmentClass = AboutAppFragment.class;
    }

    try {
      fragment = (Fragment) fragmentClass.newInstance();
      if (fragment instanceof ChooseCityFragment) {
        ((ChooseCityFragment) fragment).setOnCitySelectedListener(this);
      }
    } catch (Exception e) {
      e.printStackTrace();
    }

    FragmentManager fragmentManager = getSupportFragmentManager();
    fragmentManager.beginTransaction().replace(R.id.container, fragment).commit();
    item.setChecked(true);
    setTitle(item.getTitle());

    DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
    drawer.closeDrawer(GravityCompat.START);
    return true;
  }

  @Override
  public void onFragmentInteraction(Uri uri) {

  }

  @Override
  public void onCitySelected() {
    replaceFragment();
  }

  private void replaceFragment() {
    Fragment fragment = null;
    Class fragmentClass = ChooseWaterFragment.class;
    try {
      fragment = (Fragment) fragmentClass.newInstance();
    } catch (Exception e) {
      e.printStackTrace();
    }

    FragmentManager fragmentManager = getSupportFragmentManager();
    fragmentManager.beginTransaction().replace(R.id.container, fragment).commit();
    setTitle("Заказать воду");
  }
}
