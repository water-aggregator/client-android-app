package com.watergenerator.settings;

import android.content.Context;
import android.content.SharedPreferences;

public class Preferences {
  private static final String PREFERENCES_NAME = "watter_aggregator";

  private static final String CITY_ID = "city_id";

  private static Preferences sInstance = null;

  private final SharedPreferences mPreferences;

  private Preferences(Context context) {
    mPreferences = context.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);
  }

  public static void initInstance(Context context) {
    if (sInstance == null) {
      synchronized (Preferences.class) {
        if (sInstance == null) {
          sInstance = new Preferences(context);
        }
      }
    }
  }

  public static Preferences getInstance() {
    return sInstance;
  }

  public void saveCityId(String id) {
    SharedPreferences.Editor editor = mPreferences.edit();
    editor.putString(CITY_ID, id).apply();
  }

  public String getCityId() {
    return mPreferences.getString(CITY_ID, "0");
  }

  public boolean isCityInit() {
    return mPreferences.contains(CITY_ID);
  }
}
