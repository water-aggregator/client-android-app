package com.watergenerator.dao;

import androidx.room.Dao;
import androidx.room.Query;
import com.watergenerator.entity.Service;

import java.util.List;

@Dao
public interface ServiceDao extends BaseDao<Service> {
  @Query("SELECT * FROM " + Service.TABLE_NAME + " WHERE _id = :id")
  Service getService(Integer id);

  @Query("SELECT * FROM " + Service.TABLE_NAME)
  List<Service> getServices();

  @Query("SELECT * FROM " + Service.TABLE_NAME + " WHERE " + Service.COLUMN_CITY_ID + " = :cityId")
  List<Service> getServicesByCity(Integer cityId);
}
