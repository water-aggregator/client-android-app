package com.watergenerator.dao;

import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Update;
import com.watergenerator.entity.BaseEntity;

public interface BaseDao<EntityT extends BaseEntity> {
  @Insert
  void insert(EntityT entity);

  @Update
  void update(EntityT entity);

  @Delete
  void delete(EntityT entity);
}
