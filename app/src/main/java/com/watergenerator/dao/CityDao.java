package com.watergenerator.dao;

import androidx.room.Dao;
import androidx.room.Query;
import com.watergenerator.entity.City;

import java.util.List;

@Dao
public interface CityDao extends BaseDao<City> {
  @Query("SELECT * FROM " + City.TABLE_NAME + " WHERE _id = :id")
  City getCity(Integer id);

  @Query("SELECT * FROM " + City.TABLE_NAME)
  List<City> getCities();
}
