package com.watergenerator.dao;

import com.watergenerator.R;
import com.watergenerator.entity.City;
import com.watergenerator.entity.Contacts;
import com.watergenerator.entity.WatterInfo;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class DataProvider {
  public List<City> getCities() {
    List<City> cities = new ArrayList<>(3);
    cities.add(new City(0, "Иваново", 37));
//    cities.add(new City(1, "Владимир", 33));
//    cities.add(new City(2, "Ярославль", 67));

    return cities;
  }

  public ArrayList<WatterInfo> getWatters() {
    ArrayList<WatterInfo> watters = new ArrayList<>();
    Contacts contacts = new Contacts("http://aero-aqua.ru/", "84932340932", "", "");
    watters.add(new WatterInfo("Аэро", contacts, 0, 0, 160, R.mipmap.aero_logo));

    contacts = new Contacts("http://aero-aqua.ru/", "84932340932", "", "");
    watters.add(new WatterInfo("Аэро", contacts, 0, 0, 190, R.mipmap.aero_logo));

    contacts = new Contacts("http://aero-aqua.ru/", "84932340932", "", "");
    watters.add(new WatterInfo("Аэро", contacts, 1, 0,  120, R.mipmap.aero_logo));

    contacts = new Contacts("http://aero-aqua.ru/", "84932340932", "", "");
    watters.add(new WatterInfo("Аэро", contacts, 1, 0, 160, R.mipmap.aero_logo));

    contacts = new Contacts("http://arktika37.ru", "89303523486", "", "");
    watters.add(new WatterInfo("Арктика", contacts, 0, 0, 180, R.mipmap.arktica_logo));

    contacts = new Contacts("http://arktika37.ru", "89303523486", "", "");
    watters.add(new WatterInfo("Арктика", contacts, 1, 0, 110, R.mipmap.arktica_logo));

    contacts = new Contacts("http://h2o-37.ru/", "89022433949", "", "");
    watters.add(new WatterInfo("H2O", contacts, 0, 0, 120, R.mipmap.aero_logo));

    contacts = new Contacts("http://h2o-37.ru/", "89022433949", "", "");
    watters.add(new WatterInfo("H2O", contacts, 1, 0, 120, R.mipmap.aero_logo));

    contacts = new Contacts("http://iv-voda.ru/", "84932369000", "", "");
    watters.add(new WatterInfo("Иваново-Вознесенская", contacts, 0, 0, 210, R.mipmap.dara_logo));

    contacts = new Contacts("http://iv-voda.ru/", "84932369000", "", "");
    watters.add(new WatterInfo("Иваново-Вознесенская", contacts, 1, 0,130, R.mipmap.dara_logo));

    contacts = new Contacts("http://ivanovo.jevea.ru/", "84932360800", "", "");
    watters.add(new WatterInfo("Жевея", contacts, 1, 0, 160, R.mipmap.jevea_logo));

    return watters;
  }

  public ArrayList<WatterInfo> getWatters(Integer type) {
    ArrayList<WatterInfo> watters = new ArrayList<>(getWatters());
    Iterator<WatterInfo> iterator = watters.iterator();

    if (type == null) {
      return watters;
    }
    while (iterator.hasNext()) {
      if (iterator.next().getTaraType() != type) {
        iterator.remove();
      }
    }

    return watters;
  }
}
