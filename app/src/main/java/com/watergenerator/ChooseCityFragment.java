package com.watergenerator;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import com.watergenerator.dao.DataProvider;
import com.watergenerator.entity.City;
import com.watergenerator.settings.Preferences;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ChooseCityFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ChooseCityFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ChooseCityFragment extends Fragment {
  private Button btn_ok;
  DrawerLayout drawer;
  // TODO: Rename parameter arguments, choose names that match
  // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
  private static final String ARG_PARAM1 = "param1";
  private static final String ARG_PARAM2 = "param2";

  // TODO: Rename and change types of parameters
  private String mParam1;
  private String mParam2;

  private OnFragmentInteractionListener mListener;

  private Spinner mSpinner;

  public ChooseCityFragment() {
    // Required empty public constructor
  }

  private OnCitySelectedListener mOnCitySelectedListener;

  /**
   * Use this factory method to create a new instance of
   * this fragment using the provided parameters.
   *
   * @param param1 Parameter 1.
   * @param param2 Parameter 2.
   * @return A new instance of fragment ChooseCityFragment.
   */
  // TODO: Rename and change types and number of parameters
  public static ChooseCityFragment newInstance(String param1, String param2) {
    ChooseCityFragment fragment = new ChooseCityFragment();
    Bundle args = new Bundle();
    args.putString(ARG_PARAM1, param1);
    args.putString(ARG_PARAM2, param2);
    fragment.setArguments(args);
    return fragment;
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    if (getArguments() != null) {
      mParam1 = getArguments().getString(ARG_PARAM1);
      mParam2 = getArguments().getString(ARG_PARAM2);
    }
  }

  public void setOnCitySelectedListener(OnCitySelectedListener listener) {
    mOnCitySelectedListener = listener;
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    View rootView = inflater.inflate(R.layout.fragment_choose_city, container, false);
    drawer = this.getActivity().findViewById(R.id.drawer_layout);
    drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
    btn_ok = rootView.findViewById(R.id.btn_city_ok);
    btn_ok.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Preferences.getInstance().saveCityId(Integer.toString(mSpinner.getSelectedItemPosition()));
        ((AppCompatActivity) getActivity()).getSupportActionBar().show();
        if (mOnCitySelectedListener != null) {
          mOnCitySelectedListener.onCitySelected();
        }
      }
    });

    DataProvider provider = new DataProvider();
    List<City> cities = provider.getCities();
    List<String> cityNames = new ArrayList<>();
    for (int i = 0; i < cities.size(); i++) {
      cityNames.add(cities.get(i).toString());
    }

    mSpinner = (Spinner) rootView.findViewById(R.id.spin_city);

    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.city_item, cityNames);
    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

    mSpinner.setAdapter(adapter);
    mSpinner.setSelection(Integer.parseInt(Preferences.getInstance().getCityId()));

    return rootView;
  }

  // TODO: Rename method, update argument and hook method into UI event
  public void onButtonPressed(Uri uri) {
    if (mListener != null) {
      mListener.onFragmentInteraction(uri);
    }
  }

  @Override
  public void onAttach(Context context) {
    super.onAttach(context);
    if (context instanceof OnFragmentInteractionListener) {
      mListener = (OnFragmentInteractionListener) context;
    } else {
      throw new RuntimeException(context.toString()
          + " must implement OnFragmentInteractionListener");
    }
  }

  @Override
  public void onDetach() {
    super.onDetach();
    mListener = null;
  }

  /**
   * This interface must be implemented by activities that contain this
   * fragment to allow an interaction in this fragment to be communicated
   * to the activity and potentially other fragments contained in that
   * activity.
   * <p>
   * See the Android Training lesson <a href=
   * "http://developer.android.com/training/basics/fragments/communicating.html"
   * >Communicating with Other Fragments</a> for more information.
   */
  public interface OnFragmentInteractionListener {
    // TODO: Update argument type and name
    void onFragmentInteraction(Uri uri);
  }
}
