package com.watergenerator.entity;

import androidx.room.ColumnInfo;
import androidx.room.PrimaryKey;

public class BaseEntity<IdT> {
  protected static final String COLUMN_ID = "_id";

  @PrimaryKey(autoGenerate = true)
  @ColumnInfo(name = COLUMN_ID)
  private IdT mId;

  public IdT getId() {
    return mId;
  }

  public BaseEntity(IdT id) {
    this.mId = id;
  }
}
