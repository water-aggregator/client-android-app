package com.watergenerator.entity;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;

@Entity(tableName = City.TABLE_NAME)
public class City extends BaseEntity<Integer> {
  public static final String TABLE_NAME = "cities";
  private static final String COLUMN_NAME = "name";
  private static final String COLUMN_CODE = "code";

  @ColumnInfo(name = COLUMN_NAME)
  private String mName;
  @ColumnInfo(name = COLUMN_CODE)
  private int mCode;

  public City(Integer id, String name, int code) {
    super(id);
    mName = name;
    mCode = code;
  }

  public String getName() {
    return mName;
  }

  public int getCode() {
    return mCode;
  }

  @NonNull
  @Override
  public String toString() {
    return mName;
  }
}
