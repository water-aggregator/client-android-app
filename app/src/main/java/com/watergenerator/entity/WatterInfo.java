package com.watergenerator.entity;

import androidx.annotation.DrawableRes;

public class WatterInfo {
  public static final int DISPOSABLE_TARA = 0;
  public static final int RETURNABLE_TARA = 1;

  private String mName;
  private Contacts mContacts;
  private int mTaraType;
  private int mCityId;
  private float mPrice;
  @DrawableRes private int mLogoResId;

  public WatterInfo(
      String name,
      Contacts contacts,
      int taraType,
      int cityId,
      float price,
      int logoResId) {
    mName = name;
    mContacts = contacts;
    mTaraType = taraType;
    mCityId = cityId;
    mPrice = price;
    mLogoResId = logoResId;
  }

  public float getPrice() {
    return mPrice;
  }

  public void setPrice(float price) {
    mPrice = price;
  }

  public void setName(String name) {
    mName = name;
  }

  public void setContacts(Contacts contacts) {
    mContacts = contacts;
  }

  public void setTaraType(int taraType) {
    mTaraType = taraType;
  }

  public void setCityId(int cityId) {
    mCityId = cityId;
  }

  public String getName() {
    return mName;
  }

  public Contacts getContacts() {
    return mContacts;
  }

  public int getTaraType() {
    return mTaraType;
  }

  public int getCityId() {
    return mCityId;
  }

  public int getLogoResId() {
    return mLogoResId;
  }

  public String getReadableType() {
    return mTaraType == 0 ? "Одноразовая" : "Оборотная";
  }
}
