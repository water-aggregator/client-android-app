package com.watergenerator.entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;

@Entity(tableName = Product.TABLE_NAME)
public class Product extends BaseEntity<Integer> {
  public static final int DISPOSABLE_TARA = 0;
  public static final int RETURNABLE_TARA = 1;

  public static final String TABLE_NAME = "products";
  private static final String COLUMN_NAME = "name";
  private static final String COLUMN_IMAGE_NAME = "image_name";
  private static final String COLUMN_TARA_TYPE = "tara_type";

  @ColumnInfo(name = COLUMN_NAME)
  private String mName;
  @ColumnInfo(name = COLUMN_IMAGE_NAME)
  private String mImageName;
  @ColumnInfo(name = COLUMN_TARA_TYPE)
  private int mTaraType;

  public Product(Integer id, String name, String imageName, int taraType) {
    super(id);
    this.mName = name;
    this.mImageName = imageName;
    this.mTaraType = taraType;
  }

  public String getName() {
    return mName;
  }

  public String getImageName() {
    return mImageName;
  }

  public int getTaraType() {
    return mTaraType;
  }
}
