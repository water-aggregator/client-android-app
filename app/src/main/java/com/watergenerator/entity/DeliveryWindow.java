package com.watergenerator.entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;

@Entity(tableName = DeliveryWindow.TABLE_NAME,
    foreignKeys = @ForeignKey(entity = Service.class, parentColumns = Service.COLUMN_ID,
        childColumns = DeliveryWindow.COLUMN_SERVICE_ID))
public class DeliveryWindow extends BaseEntity<Integer> {
  public static final String TABLE_NAME = "delivery_windows";
  private static final String COLUMN_START_PERIOD_TIME = "start_period_time";
  private static final String COLUMN_FINISH_PERIOD_TIME = "start_period_time";
  protected static final String COLUMN_SERVICE_ID = "service_id";

  @ColumnInfo(name = COLUMN_START_PERIOD_TIME)
  private String mStartPeriodTime;
  @ColumnInfo(name = COLUMN_FINISH_PERIOD_TIME)
  private String mFinishPeriodTime;
  @ColumnInfo(name = COLUMN_SERVICE_ID)
  private Integer mServiceId;

  public DeliveryWindow(Integer id, String startPeriodTime, String finishPeriodTime,
                        Integer serviceId) {
    super(id);
    mStartPeriodTime = startPeriodTime;
    mFinishPeriodTime = finishPeriodTime;
    mServiceId = serviceId;
  }

  public String getStartPeriodTime() {
    return mStartPeriodTime;
  }

  public String getFinishPeriodTime() {
    return mFinishPeriodTime;
  }

  public Integer getServiceId() {
    return mServiceId;
  }
}
