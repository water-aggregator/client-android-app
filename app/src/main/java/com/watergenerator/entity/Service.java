package com.watergenerator.entity;

import androidx.room.ColumnInfo;
import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.ForeignKey;

@Entity(tableName = Service.TABLE_NAME,
    foreignKeys = @ForeignKey(entity = City.class, parentColumns = City.COLUMN_ID,
        childColumns = Service.COLUMN_CITY_ID))
public class Service extends BaseEntity<Integer> {
  public static final String TABLE_NAME = "services";
  private static final String COLUMN_NAME = "name";
  private static final String COLUMN_START_TIME_OF_WORK = "start_time_of_work";
  private static final String COLUMN_FINISH_TIME_OF_WORK = "finish_time_of_work";
  private static final String COLUMN_ALLOWED_EXPRESS_DELIVERY = "allowed_express_delivery";
  private static final String COLUMN_EXPRESS_DELIVERY_PRICE = "express_delivery_price";
  public static final String COLUMN_CITY_ID = "city_id";

  @ColumnInfo(name = COLUMN_NAME)
  private String mName;
  @ColumnInfo(name = COLUMN_START_TIME_OF_WORK)
  private String mStartTimeOfWork;
  @ColumnInfo(name = COLUMN_FINISH_TIME_OF_WORK)
  private String mFinishTimeOfWork;
  @ColumnInfo(name = COLUMN_ALLOWED_EXPRESS_DELIVERY)
  private boolean mAllowedExpressDelivery;
  @ColumnInfo(name = COLUMN_EXPRESS_DELIVERY_PRICE)
  private float mExpressDeliveryPrice;
  @ColumnInfo(name = COLUMN_CITY_ID)
  private int mCityId;
  @Embedded
  private Contacts mContacts;

  public Service(Integer id,
                 String name,
                 String startTimeOfWork,
                 String finishTimeOfWork,
                 boolean allowedExpressDelivery,
                 float expressDeliveryPrice,
                 int cityId,
                 Contacts contacts) {
    super(id);
    mName = name;
    mStartTimeOfWork = startTimeOfWork;
    mFinishTimeOfWork = finishTimeOfWork;
    mAllowedExpressDelivery = allowedExpressDelivery;
    mExpressDeliveryPrice = expressDeliveryPrice;
    mCityId = cityId;
    mContacts = contacts;
  }

  public String getName() {
    return mName;
  }

  public String getStartTimeOfWork() {
    return mStartTimeOfWork;
  }

  public String getFinishTimeOfWork() {
    return mFinishTimeOfWork;
  }

  public boolean isAllowedExpressDelivery() {
    return mAllowedExpressDelivery;
  }

  public float getExpressDeliveryPrice() {
    return mExpressDeliveryPrice;
  }

  public int getCityId() {
    return mCityId;
  }

  public Contacts getContacts() {
    return mContacts;
  }
}
