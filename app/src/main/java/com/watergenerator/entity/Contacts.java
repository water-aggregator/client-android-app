package com.watergenerator.entity;

import androidx.room.ColumnInfo;

public class Contacts {
  private static final String COLUMN_WEBSITE = "web_site";
  private static final String COLUMN_PHONE_NUMBER = "phone_number";
  private static final String COLUMN_VIBER_CHAT_URL = "viber_chat_utl";
  private static final String COLUMN_WHATSAPP_CHAT_URL = "whatsapp_chat_url";


  @ColumnInfo(name = COLUMN_WEBSITE)
  private String mWebSite;
  @ColumnInfo(name = COLUMN_PHONE_NUMBER)
  private final String mPhoneNumber;
  @ColumnInfo(name = COLUMN_VIBER_CHAT_URL)
  private final String mViberChatUrl;
  @ColumnInfo(name = COLUMN_WHATSAPP_CHAT_URL)
  private final String mWhatsApp;

  public Contacts(String webSite, String phoneNumber, String viberChatUrl, String whatsApp) {
    mWebSite = webSite;
    mPhoneNumber = phoneNumber;
    mViberChatUrl = viberChatUrl;
    mWhatsApp = whatsApp;
  }

  public String getWebSite() {
    return mWebSite;
  }

  public String getPhoneNumber() {
    return mPhoneNumber;
  }

  public String getViberChatUrl() {
    return mViberChatUrl;
  }

  public String getWhatsApp() {
    return mWhatsApp;
  }
}
