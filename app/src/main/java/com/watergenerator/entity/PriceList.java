package com.watergenerator.entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;

@Entity(tableName = PriceList.TABLE_NAME,
    foreignKeys = {
        @ForeignKey(entity = Service.class, parentColumns = Service.COLUMN_ID,
            childColumns = PriceList.COLUMN_SERVICE_ID),
        @ForeignKey(entity = Product.class, parentColumns = Product.COLUMN_ID,
            childColumns = PriceList.COLUMN_PRODUCT_ID)
    })
public class PriceList extends BaseEntity<Integer> {
  public static final String TABLE_NAME = "price_lists";
  protected static final String COLUMN_SERVICE_ID = "service_id";
  protected static final String COLUMN_PRODUCT_ID = "product_id";
  private static final String COLUMN_PRICE = "price";

  @ColumnInfo(name = COLUMN_SERVICE_ID)
  private Integer mServiceId;
  @ColumnInfo(name = COLUMN_PRODUCT_ID)
  private Integer mProductId;
  @ColumnInfo(name = COLUMN_PRICE)
  private float mPrice;

  public PriceList(Integer id, Integer serviceId, Integer productId, float price) {
    super(id);
    mServiceId = serviceId;
    mProductId = productId;
    mPrice = price;
  }

  public Integer getServiceId() {
    return mServiceId;
  }

  public Integer getProductId() {
    return mProductId;
  }

  public float getPrice() {
    return mPrice;
  }
}
