package com.watergenerator.entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;

@Entity(tableName = Order.TABLE_NAME,
    foreignKeys = {
        @ForeignKey(entity = PriceList.class, parentColumns = PriceList.COLUMN_ID,
            childColumns = Order.COLUMN_PRICE_LIST_ID),
        @ForeignKey(entity = DeliveryWindow.class, parentColumns = DeliveryWindow.COLUMN_ID,
            childColumns = Order.COLUMN_DELIVERY_WINDOW_ID)
    })
public class Order extends BaseEntity<Integer> {
  public static final String TABLE_NAME = "orders";
  private static final String COLUMN_NUMBER = "number";
  private static final String COLUMN_DATE = "date";
  private static final String COLUMN_ADDRESS = "address";
  protected static final String COLUMN_PRICE_LIST_ID = "price_list_id";
  protected static final String COLUMN_DELIVERY_WINDOW_ID = "delivery_window_id";

  @ColumnInfo(name = COLUMN_NUMBER)
  private Integer mNumber;
  @ColumnInfo(name = COLUMN_DATE)
  private String mDate;
  @ColumnInfo(name = COLUMN_ADDRESS)
  private String mAddress;
  @ColumnInfo(name = COLUMN_PRICE_LIST_ID)
  private Integer mPriceListId;
  @ColumnInfo(name = COLUMN_DELIVERY_WINDOW_ID)
  private Integer mDeliveryWindowId;

  public Order(Integer id, Integer number, String date, String address,
               Integer priceListId, Integer deliveryWindowId) {
    super(id);
    mNumber = number;
    mDate = date;
    mAddress = address;
    mPriceListId = priceListId;
    mDeliveryWindowId = deliveryWindowId;
  }

  public Integer getNumber() {
    return mNumber;
  }

  public String getDate() {
    return mDate;
  }

  public String getAddress() {
    return mAddress;
  }

  public Integer getPriceListId() {
    return mPriceListId;
  }

  public Integer getDeliveryWindowId() {
    return mDeliveryWindowId;
  }
}
