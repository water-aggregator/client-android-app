package com.watergenerator.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;
import com.watergenerator.dao.CityDao;
import com.watergenerator.dao.ServiceDao;
import com.watergenerator.entity.City;
import com.watergenerator.entity.Service;

@Database(entities = {City.class, Service.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {
  private static final String DATABASE_NAME = "water_aggregator_db";

  private static AppDatabase sInstance;

  public AppDatabase getInstance() {
    return sInstance;
  }

  public void initInstance(Context context) {
    if (sInstance == null) {
      sInstance = Room.databaseBuilder(context.getApplicationContext(),
          AppDatabase.class, DATABASE_NAME)
          .addCallback(new Callback() {
            @Override
            public void onCreate(SupportSQLiteDatabase db) {
              super.onCreate(db);
              // Temporarily.
              insertCity(db, "Иваново", 37);
              insertCity(db, "Владимир", 33);
              insertCity(db, "Ярославль", 67);

              insertSevice(db, "Водичка", true, 50, 1,
                  "www.watter.ru", "87779993545", "chat1", "chat1");
              insertSevice(db, "Много водички", false, 0, 1,
                  "www.much_watter.ru", "87779993545", "chat1", "chat1");
              insertSevice(db, "Еще больше водички", true, 100, 1,
                  "www.very_much_watter.ru", "87779993545", "chat1", "chat1");
            }
          })
          .allowMainThreadQueries()
          .build();
    }
  }

  private void insertCity(SupportSQLiteDatabase db, String name, int code) {
    ContentValues values = new ContentValues();
    values.put("name", name);
    values.put("code", code);
    db.insert(City.TABLE_NAME, SQLiteDatabase.CONFLICT_REPLACE, values);
  }

  private void insertSevice(SupportSQLiteDatabase db, String name, boolean express,
                            float expressPrice, Integer cityId, String site, String phone,
                            String viber, String whatsapp) {
    ContentValues values = new ContentValues();
    values.put("name", name);
    values.put("start_time_of_work", "");
    values.put("finish_time_of_work", "");
    values.put("allowed_express_delivery", express);
    values.put("express_delivery_price", expressPrice);
    values.put("city_id", cityId);
    values.put("web_site", site);
    values.put("phone_number", phone);
    values.put("viber_chat_utl", viber);
    values.put("whatsapp_chat_url", whatsapp);
    db.insert(Service.TABLE_NAME, SQLiteDatabase.CONFLICT_REPLACE, values);
  }

  public abstract CityDao getCityDao();

  public abstract ServiceDao getServiceDao();
}
