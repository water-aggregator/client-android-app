package com.watergenerator;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import android.util.SparseBooleanArray;
import android.view.*;

import androidx.annotation.NonNull;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.watergenerator.dao.DataProvider;
import com.watergenerator.entity.WatterInfo;
import com.watergenerator.ui.adapter.ProductAdapter;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ChooseWaterFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ChooseWaterFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ChooseWaterFragment extends Fragment implements OnMenuClickListener {
  DrawerLayout drawer;
  // TODO: Rename parameter arguments, choose names that match
  // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
  private static final String ARG_PARAM1 = "param1";
  private static final String ARG_PARAM2 = "param2";

  // TODO: Rename and change types of parameters
  private String mParam1;
  private String mParam2;

  private OnFragmentInteractionListener mListener;

  private RecyclerView mRecyclerView;

  private SparseBooleanArray mFilters = new SparseBooleanArray();

  public ChooseWaterFragment() {
    // Required empty public constructor
  }

  /**
   * Use this factory method to create a new instance of
   * this fragment using the provided parameters.
   *
   * @param param1 Parameter 1.
   * @param param2 Parameter 2.
   * @return A new instance of fragment ChooseWaterFragment.
   */
  // TODO: Rename and change types and number of parameters
  public static ChooseWaterFragment newInstance(String param1, String param2) {
    ChooseWaterFragment fragment = new ChooseWaterFragment();
    Bundle args = new Bundle();
    args.putString(ARG_PARAM1, param1);
    args.putString(ARG_PARAM2, param2);
    fragment.setArguments(args);
    return fragment;
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    if (getArguments() != null) {
      mParam1 = getArguments().getString(ARG_PARAM1);
      mParam2 = getArguments().getString(ARG_PARAM2);
    }
    mFilters.put(0, true);
    mFilters.put(1, true);
    setHasOptionsMenu(true);
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    // Inflate the layout for this fragment
    View view = inflater.inflate(R.layout.fragment_choose_water, container, false);
    mRecyclerView = (RecyclerView) view.findViewById(R.id.watter_list);

    mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
    drawer = this.getActivity().findViewById(R.id.drawer_layout);
    drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNDEFINED);
    ProductAdapter adapter = new ProductAdapter(null);
    DataProvider provider = new DataProvider();
    ArrayList<WatterInfo> watterList = provider.getWatters();
    adapter.setItems(watterList);
    mRecyclerView.setAdapter(adapter);

    return view;
  }

  @Override
  public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
    super.onCreateOptionsMenu(menu, inflater);
    inflater.inflate(R.menu.main_activity, menu);
  }

  @Override
  public boolean onOptionsItemSelected(@NonNull MenuItem item) {
    int id = item.getItemId();
    item.setChecked(!item.isChecked());

    //noinspection SimplifiableIfStatement
    if (id == R.id.action_once) {
      if (mListener != null) {
        onActionOnceClick();
      }
      return true;
    }
    if (id == R.id.action_refresh) {
      if (mListener != null) {
        onActionRefreshClick();
      }
      return true;
    }

    return super.onOptionsItemSelected(item);
  }

  // TODO: Rename method, update argument and hook method into UI event
  public void onButtonPressed(Uri uri) {
    if (mListener != null) {
      mListener.onFragmentInteraction(uri);
    }
  }

  @Override
  public void onAttach(Context context) {
    super.onAttach(context);
    if (context instanceof OnFragmentInteractionListener) {
      mListener = (OnFragmentInteractionListener) context;
    } else {
      throw new RuntimeException(context.toString()
          + " must implement OnFragmentInteractionListener");
    }
  }

  @Override
  public void onDetach() {
    super.onDetach();
    mListener = null;
  }

  @Override
  public void onActionOnceClick() {
    mFilters.put(0, !mFilters.get(0));
    filter();
  }

  @Override
  public void onActionRefreshClick() {
    mFilters.put(1, !mFilters.get(1));
    filter();
  }

  private void filter() {
    ProductAdapter adapter = new ProductAdapter(null);
    DataProvider provider = new DataProvider();
    ArrayList<WatterInfo> watterList;
    if (mFilters.get(0) && mFilters.get(1)) {
      watterList = provider.getWatters(null);
    } else if (mFilters.get(0)) {
      watterList = provider.getWatters(0);
    } else if (mFilters.get(1)) {
      watterList = provider.getWatters(1);
    } else {
      watterList = new ArrayList<>();
    }

    adapter.setItems(watterList);
    mRecyclerView.setAdapter(adapter);
  }

  /**
   * This interface must be implemented by activities that contain this
   * fragment to allow an interaction in this fragment to be communicated
   * to the activity and potentially other fragments contained in that
   * activity.
   * <p>
   * See the Android Training lesson <a href=
   * "http://developer.android.com/training/basics/fragments/communicating.html"
   * >Communicating with Other Fragments</a> for more information.
   */
  public interface OnFragmentInteractionListener {
    // TODO: Update argument type and name
    void onFragmentInteraction(Uri uri);
  }
}
