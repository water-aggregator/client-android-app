package com.watergenerator;

public interface OnCitySelectedListener {
  void onCitySelected();
}
