package com.watergenerator;

public interface OnMenuClickListener {
  void onActionOnceClick();
  void onActionRefreshClick();
}
